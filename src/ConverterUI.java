import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
/**
 * Converter Graphical User Interface.
 * @author Arut Thanomwatana
 *
 */
public class ConverterUI extends JFrame
{
	// attributes for graphical components
	private JButton convertButton;
	private JButton clearButton;
	private JTextField inputField1;
	private JTextField inputField2;
	private JLabel equals;
	private JComboBox unit1ComboBox;
	private JComboBox unit2ComboBox;
	private UnitConverter unitconverter;

	/**
	 * Initialize the Converter UI.
	 * @param uc is the UnitConverter
	 */
	public ConverterUI( UnitConverter uc ) {
		this.unitconverter = uc;
		this.setTitle("Length Converter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponents( );
		this.setVisible(true);
	}

	/**
	 * Initialize components in the window.
	 */
	private void initComponents() {

		inputField1 = new JTextField(5);
		inputField2 = new JTextField(5);
		inputField2.setEditable(false);

		equals = new JLabel("=");

		unit1ComboBox = new JComboBox<Unit>();
		Unit [] lengths = unitconverter.getUnits();
		for(Unit u : lengths) unit1ComboBox.addItem(u);

		unit2ComboBox = new JComboBox<Unit>(lengths);

		clearButton = new JButton("Clear");

		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout( );
		contents.setLayout( layout );
		convertButton = new JButton("Convert");

		contents.add(inputField1);
		contents.add(unit1ComboBox);
		contents.add(equals);
		contents.add(inputField2);
		contents.add(unit2ComboBox);
		contents.add(convertButton);
		contents.add(clearButton);

		ActionListener convert = new ConvertButtonListener( );
		ActionListener clear = new ClearButtonListener();
		convertButton.addActionListener( convert );
		inputField1.addActionListener(convert);
		clearButton.addActionListener(clear);

		this.pack(); 
	}
	/** Convert the value in textfield1 and sent the converted 
	 *  value to the textfield2 
	 */
	class ConvertButtonListener implements ActionListener {

		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			String s = inputField1.getText().trim();

			//System.out.println("actionPerformed: input=" + s);
			if ( s.length() > 0 ) {
				try{
					inputField1.setBackground(Color.WHITE);
					double value = Double.valueOf( s );
					String result = Double.toString(unitconverter.convert(value,(Unit)unit1ComboBox.getSelectedItem(),(Unit)unit2ComboBox.getSelectedItem()));
					inputField2.setText(result);

				} catch(NumberFormatException e){
					inputField1.setBackground(Color.red);
					inputField2.setText("");
				}
			}
		}
	} 
	/** Clear the text field into its default. */
	class ClearButtonListener implements ActionListener{

		public void actionPerformed(ActionEvent evt){
			inputField1.setText("");
			inputField1.setBackground(Color.WHITE);
			inputField2.setText("");
		}
	}
}
