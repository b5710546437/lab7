/**
 * A UnitConverter that can convert from 1 to another Unit.
 * @author Arut Thanomwatana
 *
 */
public class UnitConverter 
{
	/**
	 * Convert value from 1 Unit to another Unit.
	 * @param amount is an amount that want to be converted
	 * @param fromUnit is the unit of the amount that want to be converted
	 * @param toUnit is the unit of the amount that will be converted
	 * @return value that already converted
	 */
	public double convert(double amount, Unit fromUnit,Unit toUnit)
	{
		return amount*fromUnit.getValue()/toUnit.getValue();
	}
	
	/**
	 * Get all unit in the Length
	 * @return Array of all the unit in length
	 */
	public Unit[] getUnits(){
		return Length.values();
	}

}
