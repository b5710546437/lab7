/**
 * An interface of the object that implements unit
 * @author Arut Thanomwatana
 *
 */
public interface Unit 
{
	/** Convert the value of 1 unit to another unit .
	 *@param amt is the amount that going to be converted
	 *@param unit is the unit that will be convert to 
	 */
	public double convertTo(double amt,Unit unit);
	/** Get the value of the unit */
	public double getValue();
	/** Define the Unit */
	public String toString();

}
