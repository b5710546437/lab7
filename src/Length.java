/**
 * An enum of the length that have convert ability.
 * @author Arut Thanomwatana
 *
 */
public enum Length implements Unit
{
	METER("Meter",1.0),
	KILOMETER("Km",1000.0),
	MILE("Mile",1609.3344),
	WA("Wa",2.0),
	CENTIMETER("Centimeter",0.01),
	FOOT("Foot",0.30480),
	MICRON("Micron",1.0E-6),
	LIGHTYEAR("Light-year",9460730472580800.0);
	
	/**Name of the Unit. */
	public final String NAME;
	/** Value in one Unit. */
	public final double VAL;
	
	/**
	 * Initialize enum Length
	 * @param name is a name of the unit.
	 * @param val is a value in 1 unit.
	 */
	private Length(String name,double val)
	{
		this.NAME = name;
		this.VAL = val;
	}
	
	/**
	 * Define the Unit.
	 * @return name of the Unit
	 */
	public String toString()
	{
		return this.NAME;
	}
	
	/**
	 * Convert value form 1 unit to another unit.
	 * @param amt is a amount that want to be converted
	 * @param unit is a unit that will be converted to
	 */
	public double convertTo(double amt,Unit unit)
	{
		return this.VAL*amt/unit.getValue();
	}

	/**
	 * Get the value of the Unit.
	 * @return Value of the Unit
	 */
	public double getValue() 
	{
		return this.VAL;
	}

}
